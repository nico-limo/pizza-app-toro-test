import { db } from "../../../fbConfig";
export const ItemFunctions = () => {
  const itemRef = db.collection("Pizzas");

  const getAllPieces = async () => {
    const snapshot = await itemRef.get();
    if (snapshot.empty) {
      console.log("no pizzas found in db");
      return;
    }
    let pizzas = [];
    snapshot.forEach((doc) => {
      pizzas = [...pizzas, doc.data()];
    });
    return pizzas;
  };

  const getSinglePiece = async (id) => {
    const snapshot = await itemRef.where("flavour", "==", `${id}`).get();
    if (snapshot.empty) {
      console.log("Pizza not found it");
      return;
    }
    let pizza = "";
    snapshot.forEach((doc) => {
        pizza = doc.data();
    });
    return pizza;
  };

  return { getAllPieces, getSinglePiece };
};
