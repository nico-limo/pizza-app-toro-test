import { db } from "../../../fbConfig";
export const UserFunctions = () => {
    const usersReference = db.collection("Users");

    const newUser = async (user) => {
        await usersReference.doc().set({
          email: user.user.email,
          uid: user.user.uid,
        });
      };

      const getUser = async (id) => {
        const snapshot = await usersReference.where("uid", "==", id).get();
        if (snapshot.empty) {
          return;
        }
        let user = "";
        snapshot.forEach((doc) => {
          user = doc.data();
        });
        return user;
      };

      return {newUser, getUser};
}