//Firebase Auth
import { auth } from "../../../fbConfig";
// React Router Dom
import { useHistory } from "react-router-dom";
// Recoil
import { useSetRecoilState } from "recoil";
import { userAtom, errorAtom } from "../../../State/atoms";
//Firestore
import {UserFunctions} from "../firestore/userRequest";
export const AuthFunctions = () => {
  const {newUser, getUser} = UserFunctions();
  const history = useHistory();
  const setUser = useSetRecoilState(userAtom);
  const setError = useSetRecoilState(errorAtom);

  const login = (event, email, password) => {
    event.preventDefault();
    auth
      .signInWithEmailAndPassword(email, password)
      .then((userAuth) => getUser(userAuth.user.uid))
      .then((res) => {
        setUser({email: res.email, uid: res.uid});
        localStorage.setItem('logged', JSON.stringify({ "uid": res.uid, "email": res.email }))
      })
      .then(() => history.push("/home"))
      .catch((e) => setError(e.message))
  };

  const register = (event, email, password) => {
    event.preventDefault()
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((userAuth) => newUser(userAuth))
      .then(() => history.push("/home"))
      .catch((e) => setError(e.message))
  };

  const logged = () => auth.onAuthStateChanged( userAuth => {
    if (userAuth) {
      getUser(userAuth.uid)
      .then((res) => {
        setUser({email: res.email, uid: res.uid})
        localStorage.setItem('logged', JSON.stringify({ "uid": res.uid, "email": res.email }))
      })
      .catch(e => console.log(e))
    }
  } );
  
  const logOut = () => {
    auth.signOut();
    setUser({});
    localStorage.removeItem('logged')
    history.push("/")
  };
  return { login, register,logged, logOut };
};
