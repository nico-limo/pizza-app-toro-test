import React from 'react';
import ReactDOM from 'react-dom';
//Components
import App from './Container/App';
//Gral CSS Styles
import "../src/Styles/index.module.css";
//Recoil
import { RecoilRoot } from "recoil";

ReactDOM.render(
  <React.StrictMode>
    <RecoilRoot>
    <App />
    </RecoilRoot>
  </React.StrictMode>,
  document.getElementById('root')
);

