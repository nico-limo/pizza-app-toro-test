import React, { useEffect } from "react";
//React router Dom
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
//Containers
import LandingPage from "./LandingPage";
import Home from "./Home";
import SingleProduct from "./SingleProduct";
//Components
import Header from "../Components/Header";
//Utils
import { AuthFunctions } from "../Utils/Firebase/Auth/fbAuth";
//Recoil
import { useRecoilValue } from "recoil";
import { userAtom } from "../State/atoms";
const App = () => {
  const { logged } = AuthFunctions();
  const user = useRecoilValue(userAtom);

  useEffect(() => {
    logged();
  }, []);

  useEffect(() => {
  }, [user])
  return (
    <>
      <Router>
        <Header/>
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/home/:id" render={({match}) => <SingleProduct id={match.params.id} />} />
          {/* User view with account information and orders*/}
        </Switch>
      </Router>
    </>
  );
};

export default App;
