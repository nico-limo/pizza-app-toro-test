import React from "react";
//Components
import Form from "../Components/Form";
import Welcome from "../Components/Welcome";
//CSS
import styles from "../Styles/landing.module.css";
const LandingPage = () => {
  const user = JSON.parse(localStorage.getItem("logged"));
  return (
    <div className={styles.landing}>
      <div className={styles.left}>
        <h1>
          Piz<span>App</span>
        </h1>
        <p>Be part of the best community of Pizza.</p>

        <p>
          Here you can find all the recipes and bake it at home or buy it whith
          the ingredients you want.
        </p>
      </div>
      <div className={styles.right}>
        {user && user.uid ? <Welcome/> : <Form/> }
        
      </div>
    </div>
  );
};

export default LandingPage;
