import React, { useEffect } from "react";
//React router dom
import { useHistory } from "react-router-dom";
//CSS
import styles from "../Styles/home.module.css";
import Product from "../Components/Product";
//Utils
import { ItemFunctions } from "../Utils/Firebase/firestore/ItemRequest";
//Recoil
import { useRecoilState } from "recoil";
import { pizzasAtom } from "../State/atoms";
import Spinner from "../Components/Spinner";
const Home = () => {
  const history = useHistory();
  const user = JSON.parse(localStorage.getItem("logged"));
  const { getAllPieces } = ItemFunctions();
  const [pizzas, setPizzas] = useRecoilState(pizzasAtom);

  useEffect(() => {
    getAllPieces().then((res) => setPizzas(res));
  }, []);

  return user && user.uid ? (
    <div className={styles.home}>
      <div className={styles.subTitle}>
        <h3>Choose a flavour of pizza</h3>
      </div>
      <div className={styles.catalog}>
        {pizzas.length ? (
          pizzas.map((pizza) => (
            <Product
              key={pizza.flavour}
              flavour={pizza.flavour}
              desc={pizza.description}
              ingredients={pizza.ingredients}
              imgUrl={pizza.imgUrl}
            />
          ))
        ) : (
          <Spinner />
        )}
      </div>
    </div>
  ) : (
    <>{history.push("/")}</>
  );
};

export default Home;
