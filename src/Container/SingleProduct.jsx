import React, { useEffect, useState } from "react";
//CSS
import styles from "../Styles/singleProduct.module.css";
//Utils
import { ItemFunctions } from "../Utils/Firebase/firestore/ItemRequest";
//Recoil
import { useRecoilState } from "recoil";
import { singlePizzaAtom } from "../State/atoms";
//Components
import Spinner from "../Components/Spinner";
const SingleProduct = ({ id }) => {
  const { getSinglePiece } = ItemFunctions();
  const [pizza, setPizza] = useRecoilState(singlePizzaAtom);
  const [ingredients, setingredients] = useState([]);
  const [backList, setBackList] = useState([]);
  const [prices, setPrices] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  useEffect(() => {
    getSinglePiece(id).then((res) => {
      setPizza(res);
      setingredients(Object.keys(res.ingredients));
      setBackList(Object.keys(res.ingredients));
      setPrices(Object.values(res.ingredients));
    });
    return setPizza({});
  }, []);

  useEffect(() => {
    if(prices.length){
      let price = prices.reduce((acc,current) => acc + current);
    setTotalPrice(price)
    
    }
  }, [prices]);

  const addItem = (id) => {
    ingredients.filter((item) => {
      if (item === "Removed") {
        setTotalPrice(totalPrice + prices[id]);
        let newArr = ingredients;
        newArr[id] = backList[id];
        setingredients(newArr);
      }
    });
  };

  const removeItem = (id) => {
    ingredients.filter((item) => {
      if (item !== "Removed") {
        if (item === ingredients[id]) {
          setTotalPrice(totalPrice - prices[id]);
          let newArr = ingredients;
          newArr[id] = "Removed";
          setingredients(newArr);
        }
      }
    });
  };

  return  pizza.flavour ? (
    <div className={styles.product}>
      <div className={styles.left}>
        <h3>{pizza.flavour}</h3>
        <img src={pizza.imgUrl} alt="" />
        <p>{pizza.description}</p>
      </div>
      <div className={styles.right}>
        <h3>INGREDIENTS</h3>
        <div className={styles.titles}>
          <h4>Ingredient</h4>
          <h4>Unit Price</h4>
          <h4>ADD/REMOVE</h4>
        </div>
        {ingredients ? (
          ingredients.map((item, id) => (
            <div className={styles.list} key={id}>
              <p>{item}</p>
              <p>${prices[id]}</p>
              <div className={styles.buttons}>
                <button onClick={() => addItem(id)}>ADD</button>
                <button onClick={() => removeItem(id)}>REMOVE</button>
              </div>
            </div>
          ))
        ) : (
          <></>
        )}
        <div className={styles.checkout}>
          <p>
            Total price: <span>${totalPrice.toFixed(2)}</span>
          </p>
          <button>Buy</button>
        </div>
      </div>
    </div>
  ) : (
    <Spinner />
  );
};

export default SingleProduct;
