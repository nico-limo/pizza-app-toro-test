import { atom } from "recoil";

export const userAtom = atom({
  key: "userAtom",
  default: {},
});

export const errorAtom = atom({
  key: "errorAtom",
  default: "",
});

export const pizzasAtom = atom({
  key: "pizzasAtom",
  default: [],
});

export const singlePizzaAtom = atom({
  key: "singlePizzaAtom",
  default: {},
});
