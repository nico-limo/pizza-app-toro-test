import firebase from "firebase/app";
import "firebase/firebase-firestore";
import "firebase/firebase-auth";
import "firebase/firebase-storage";

const firebaseConfig = {
  apiKey: "AIzaSyAvEnYPZX7sQ7MYlNECaE2nln5aHNqKZ70",
  authDomain: "pizapp-toro.firebaseapp.com",
  projectId: "pizapp-toro",
  storageBucket: "pizapp-toro.appspot.com",
  messagingSenderId: "732685809028",
  appId: "1:732685809028:web:9e938429e712f457e019d1",
};

const firebaseApp = firebase.initializeApp(firebaseConfig); // Inicializas firebase de la cloud a tu proyecto
const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();
export { auth, db, storage };