import React from "react";
//CSS
import styles from "../Styles/form.module.css";
import button from "../Styles/product.module.css";
//react router dom
import { Link } from "react-router-dom";

const Welcome = () => {
  const user = JSON.parse(localStorage.getItem("logged"));

  return (
    <div className={styles.form}>
      <br />
      <br />
      <h2>Welcome back, {user.email}</h2>
      <br />
      <br />
      <div className={styles.buttons}>
        <Link to="/home" className={button.myButton}>Get your Pizza</Link>
      </div>
    </div>
  );
};

export default Welcome;
