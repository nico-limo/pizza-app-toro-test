import React from "react"
//CSS
import spinner from "../Styles/spinner.module.css";
import load from "../Utils/Images/load.gif";
const Spinner = () => {
  return (
    <div className={spinner.load}>
    <img src={load} alt=""/>
    </div>
  )
}

export default Spinner