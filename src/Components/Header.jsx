import React from "react";
//React router dom
import { Link, useHistory } from "react-router-dom";
//CSS
import styles from "../Styles/home.module.css";
//Components
import LogOut from "../Components/LogOut";

const Header = () => {
  const history = useHistory();
  const user = JSON.parse(localStorage.getItem("logged"));

  return user && user.uid ? (
    <div className={styles.home}>
      <div className={styles.header}>
        <Link to="/home" className={styles.title}>
          <h1>
            Piz<span>App</span>
          </h1>
        </Link>
        <LogOut />
      </div>
    </div>
  ) : (
    <>{history.push("/")}</>
  );
};

export default Header;
