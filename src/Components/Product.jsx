import React from "react";
//CSS
import styles from "../Styles/product.module.css";
// React Router Dom
import { Link } from "react-router-dom";

const Product = ({flavour, desc, ingredients,imgUrl}) => {
  let value = Object.values(ingredients).reduce((acc, val) => acc + val)
  value = value.toFixed(2);
  return (
    <div className={styles.product}>
      <img
        src={imgUrl}
        alt=""
      />
      <div className={styles.info}>
        <h3>{flavour}</h3>
        <p>{desc}</p>
        <p>${value}</p>
      </div>
      <Link to={`/home/${flavour}`} className={styles.myButton}>view Product</Link>
    </div>
  );
};

export default Product;
