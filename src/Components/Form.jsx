import React from "react";
//CSS
import styles from "../Styles/form.module.css";
//Utils
import {useInput} from "../Utils/Hooks/useInput";
import {AuthFunctions} from "../Utils/Firebase/Auth/fbAuth";
//Recoil
import { useRecoilState } from "recoil";
import { errorAtom } from "../State/atoms";

const Form = () => {
  const email = useInput("email");
  const password = useInput("password");
  const {login, register} = AuthFunctions();
  const [errorInput, setError] = useRecoilState(errorAtom);


  const handleClick = () => setError("");

  return  (
    <div className={styles.form}>
      <h2>Sign Up with your account or Sign In to be part of the community</h2>
      <form onClick={handleClick}>
        <input 
        type="email" 
        placeholder="Email address"
        {...email}
        />
        <input 
        type="password" 
        placeholder="Password"
        {...password}
        />
      </form>
      <div className={`${errorInput !== "" ? styles.error : ""}`}>{errorInput}</div>
      <div className={styles.buttons}>
        <button onClick={(event)=> register(event,email.value,password.value)}>Sign Up</button>
        <button onClick={(event)=> login(event,email.value,password.value)}>Sign In</button>
      </div>
    </div>
  );
};

export default Form;
