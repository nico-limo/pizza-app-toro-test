import React from "react";
//local
import logOutIcon from "../Utils/Images/logout.jpg";
//CSS
import styles from "../Styles/logOut.module.css";
//firebase
import {AuthFunctions} from "../Utils/Firebase/Auth/fbAuth";
const LogOut = () => {
  const {logOut} = AuthFunctions();
  const user = JSON.parse(localStorage.getItem("logged"));
  return user && user.uid ? 
  <div onClick={()=> logOut()} className={styles.logOut}>
  <p>LogOut</p>
  <img id={styles.logOutIcon} src={logOutIcon} alt=""/>
  </div>
   : <></>;
};

export default LogOut;
