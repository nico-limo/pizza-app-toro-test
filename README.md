# Web App Test
## Documentation about the project

1) I created a repository in bitbucket and use a create-react-app for the boilerplate of the project
2) The frameworks and tools in this project, are React Js, Firebase (Authetication, Firestore, Storage), localstorage
3) The files organization it will by:

1. Components
   - Files.jsx
2. Containers
   - Files.jsx
3. Styles
   - Css Modules
4. State
   - Atoms
   - Selectors

5. Utils
   - Firebase
     - Auth
     - Firestore DB
   - Hooks
   - Images

4) In firebase i create a web project. Then I paste the config in the file fbConfig and installed firebase.
5) Setting in the App.jsx, the views and route that I need.
6) Add it a state tool, recoil Js similar to redux.
## External Libraries & Installations
Libraries
- react-router-dom:router components for websites
- firebase: google app for development, it help me to do the authtentication and set a non-relational database.
- recoil js: Is a state manangment library very similar to Redux.

```bash
npm install
```

### Extras & Tests



